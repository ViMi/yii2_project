<?php

namespace backend\controllers;

use backend\models\forms\LoginForm;
use backend\models\forms\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class UserController extends \yii\web\Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        return [

        ];

    }




    public function actionLogin()
    {

        $model = new LoginForm();

        if($model->load(Yii::$app->request->post()) && $model->login())
        {
            Yii::$app->session->setFlash('success', 'User entered');
            return $this->redirect(['site/index']);
        }

        return $this->render
        ('login', ['model'=>$model]);

    }

    public function actionSignup()
    {


        $model = new  SignupForm();

        if($model->load(Yii::$app->request->post()) && $model->Signup())
        {
            Yii::$app->session->setFlash('success', 'User registered');
            return $this->redirect(['site/index']);
        }
        return $this->render
        ('signup',['model' => $model]);
    }




    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
