<?php

/* @var $this yii\web\view */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\forms\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Signup";
$this->params['breadcrunbs'][] = $this->title;

?>


<div class="site-sigup">
    <h1><?=Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form=ActiveForm::begin(["id"=>'form-signup']);?>
        <?= $form->field($model, "username")->textInput(['autofocus'=>true]) ?>
            <?= $form->field($model, "email") ?>
            <?= $form->field($model, "password")->passwordInput()?>
            <div class="from-group">
                <?= Html::submitButton('signup', ['class'=>'btn btn-primary', 'name'=>'signup'])?>
            </div>


            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
