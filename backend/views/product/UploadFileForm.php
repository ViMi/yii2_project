<?php
use yii\bootstrap\ActiveForm;
use yii\Helpers\Html;

$form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']])?>

<?=$form->field($model, 'imageFile')->fileInput() ?>
<?= Html::submitButton("Upload", ['class'=>'btn btn-primary', 'name'=>'upload-button'])?>
<?php ActiveForm::end()?>
