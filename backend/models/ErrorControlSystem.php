<?php


namespace backend\models\ErrorControl;


class ErrorControlSystem
{

    const ACCESS_DENIED_ERROR = "Access denied";
    const PERMISSION_DENIED_ERROR = "Permission denied";
}