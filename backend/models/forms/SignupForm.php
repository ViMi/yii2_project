<?php


namespace backend\models\forms;

use backend\models\Rbac\Roles;
use backend\models\User;
use Yii;
use yii\base\Model;

class SignupForm extends  Model
{

    public $username;
    public $email;
    public $password;


    public function rules() {


        return [
          ['username', 'trim'],
          ['username', 'required'],
          ['username', 'unique', 'targetClass'=>'\backend\models\User', 'message'=>'This user already exist'],
            ['username', 'string', 'min'=>2,'max'=>255],


            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\backend\models\User', 'message'=>'Email Already exist'],



            ['password', 'required'],
            ['password', 'string', 'min'=>Yii::$app->params['user.passwordMinLength']],

        ];
    }


    public function Signup() {


        if(!$this->validate()){
            return null;
        }

        $user = new \backend\models\User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $user->auth_key = Yii::$app->security->generateRandomString();

        $user->created_at = $time = time();
        $user->updated_at = $time;

        $user->save();


        $auth = Yii::$app->authManager;


        /*
         * JANE - SUPER ADMIN
         * JOHN - ADMIN
         * ALEX - MANAGER
         * PETER - AUT USER
         */

        $userRole =
          // $auth->getRole(Roles::SUPER_ADMIN);
         //$auth->getRole(Roles::ADMIN);
         //$auth->getRole(Roles::MANAGER);
         $auth->getRole(Roles::AUTH_USER);



        $auth->assign($userRole, $user->getId());


        return $user;

    }


}