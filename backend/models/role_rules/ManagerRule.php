<?php
namespace backend\models\Rbac;

use yii\rbac\Rule;
use backend\models\Terms;

class ManagerRule extends Rule {

    public $name = "isManager";


    public function execute($user, $item, $params)
    {


        if(isset($params[Terms\DomainAreaTerms::PRODUCT])) {
            return $params[Terms\DomainAreaTerms::PRODUCT]->createdBy == $user;
        }

        return true;

    }


}