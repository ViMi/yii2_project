<?php



namespace console\controllers;




use common\models\Rbac\Rbac;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{

    public function actionInit() {


    //    $test = Yii::$app->rbac::UPDATE_ALL_PRODUCT;

/*
        $auth = Yii::$app->authManager;



    ///GUEST PERMISSION

     //   $ruleGuest = new GuestRule();
     //  $auth->add($ruleGuest);


        $viewCategory = $auth->createPermission(Rbac::VIEW_CATEGORY);
        $viewCategory->description = Rbac::getDescription(Rbac::VIEW_CATEGORY);
        $viewCategory->ruleName = $ruleGuest->name;

        $viewProduct = $auth->createPermission(Rbac::VIEW_PRODUCT);
        $viewProduct->description = Rbac::getDescription(Rbac::VIEW_PRODUCT);
        $viewCategory->ruleName = $ruleGuest->name;


        $auth->add($viewCategory);
        $auth->add($viewProduct);


      //  $guesRole = $auth->createRole(Roles::GUEST);
       // $guesRole->description = Roles::getDescription(Roles::GUEST);
    //    $guesRole->ruleName = $ruleGuest->name;

     //   $auth->add($guesRole);

       // $auth->addChild($guesRole, $viewProduct);
      //  $auth->addChild($guesRole, $viewCategory);



    ///AUTH_USER PERMISSION
        // add To that role ---- ViewCategory
        // add To that role ---- ViewProduct

        $authorRule = new AuthorRule();
        $auth->add($authorRule);

        $viewManufacturer = $auth->createPermission(Rbac::VIEW_MANUFACTURER);
        $viewManufacturer->description = Rbac::getDescription(Rbac::VIEW_MANUFACTURER);
        $viewManufacturer->ruleName = $authorRule->name;

        $auth->add($viewManufacturer);



        $authUserRole = $auth->createRole(Roles::AUTH_USER);
        $authUserRole->description = Roles::getDescription(Roles::AUTH_USER);
        $authUserRole->ruleName = $authorRule->name;

        $auth->add($authUserRole);


        $auth->addChild($authUserRole, $viewManufacturer);
        $auth->addChild($authUserRole,$guesRole);





    ////MANAGER PERMISSION
        ///ALL Right from AuthUser Role

        $managerRule = new ManagerRule();
        $auth->add($managerRule);


        $insertProduct = $auth->createPermission(Rbac::CREATE_NEW_PRODUCT);
        $insertProduct->description = Rbac::getDescription(Rbac::CREATE_NEW_PRODUCT);
        $insertProduct->ruleName = $managerRule->name;

        $updateOwnProduct = $auth->createPermission(Rbac::UPDATE_OWN_PRODUCT);
        $updateOwnProduct->description = Rbac::getDescription(Rbac::UPDATE_OWN_PRODUCT);
        $updateOwnProduct->ruleName = $managerRule->name;

        $auth->add($insertProduct);
        $auth->add($updateOwnProduct);



        $managerRole = $auth->createRole(Roles::MANAGER);
        $managerRole->description = Roles::getDescription(Roles::MANAGER);
        $managerRole->ruleName = $managerRule->name;

        $auth->add($managerRole);


        $auth->addChild($managerRole,$authUserRole);


        $auth->addChild($managerRole,$insertProduct);
        $auth->addChild($managerRole,$updateOwnProduct);



    /////ADMIN PERMISSION
        /// ALL RIGHT from manager Role
        ///

        $adminRule = new AdminRule();
        $auth->add($adminRule);

        $updateAllProduct = $auth->createPermission(Rbac::UPDATE_ALL_PRODUCT);
        $updateAllProduct->description = Rbac::getDescription(Rbac::UPDATE_ALL_PRODUCT);
        $updateAllProduct->ruleName = $adminRule->name;

        $updateCategory = $auth->createPermission(Rbac::UPDATE_ALL_CATEGORY);
        $updateCategory->description = Rbac::getDescription(Rbac::UPDATE_ALL_CATEGORY);
        $updateCategory->ruleName = $adminRule->name;


        $insertCategory = $auth->createPermission(Rbac::CREATE_NEW_CATEGORY);
        $insertCategory->description = Rbac::getDescription(Rbac::CREATE_NEW_CATEGORY);
        $insertCategory->ruleName = $adminRule->name;


        $updateManufacturer = $auth->createPermission(Rbac::UPDATE_ALL_MANUFACTURER);
        $updateManufacturer->description = Rbac::getDescription(Rbac::UPDATE_ALL_MANUFACTURER);
        $updateManufacturer->ruleName = $adminRule->name;


        $insertManufacturer = $auth->createPermission(Rbac::CREATE_NEW_MANUFACTURER);
        $insertManufacturer->description = Rbac::getDescription(Rbac::CREATE_NEW_MANUFACTURER);
        $insertManufacturer->ruleName = $adminRule->name;


        $auth->add($updateAllProduct);
        $auth->addChild($updateAllProduct, $updateOwnProduct);
        $auth->add($updateCategory);
        $auth->add($insertCategory);
        $auth->add($updateManufacturer);
        $auth->add($insertManufacturer);





        $adminRole = $auth->createRole(Roles::ADMIN);
        $adminRole->description = Roles::getDescription(Roles::ADMIN);
        $adminRole->ruleName = $adminRule->name;

        $auth->add($adminRole);



        $auth->addChild($adminRole,$managerRole);


        $auth->addChild($adminRole, $updateAllProduct);
        $auth->addChild($adminRole, $updateCategory);
        $auth->addChild($adminRole, $insertCategory);
        $auth->addChild($adminRole, $updateManufacturer);
        $auth->addChild($adminRole, $insertManufacturer);





        ////SUPER ADMIN
        /// ALl right from admin
        ///


        $superAdminRule = new SuperAdminRule();
        $auth->add($superAdminRule);

        $deleteProduct = $auth->createPermission(Rbac::DELETE_ALL_PRODUCT);
        $deleteProduct->description = Rbac::getDescription(Rbac::DELETE_ALL_PRODUCT);
        $deleteProduct->ruleName = $superAdminRule->name;


        $deleteCategory = $auth->createPermission(Rbac::DELETE_ALL_CATEGORY);
        $deleteCategory->description = Rbac::getDescription(Rbac::DELETE_ALL_CATEGORY);;
        $deleteCategory->ruleName = $superAdminRule->name;


        $deleteManufacturer = $auth->createPermission(Rbac::DELETE_ALL_MANUFACTURER);
        $deleteManufacturer->description = Rbac::getDescription(Rbac::DELETE_ALL_MANUFACTURER);
        $deleteManufacturer->ruleName = $superAdminRule->name;


        $auth->add($deleteProduct);
        $auth->add($deleteCategory);
        $auth->add($deleteManufacturer);




        $superAdminRole = $auth->createRole(Roles::SUPER_ADMIN);
        $superAdminRole->description = Roles::getDescription(Roles::SUPER_ADMIN);
        $superAdminRole->ruleName = $superAdminRule->name;



        $auth->add($superAdminRole);



        $auth->addChild($superAdminRole,$adminRole);


        $auth->addChild($superAdminRole, $deleteProduct);
        $auth->addChild($superAdminRole, $deleteCategory);
        $auth->addChild($superAdminRole, $deleteManufacturer);


*/





    }


}
