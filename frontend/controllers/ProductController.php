<?php

namespace frontend\controllers;

use frontend\models\FileSystem;
use frontend\models\Image;
use frontend\models\UploadModel;
use Yii;
use frontend\models\Product;
use frontend\models\ProductSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access'=> AccessControl::className(),
            'only'=>['update', 'create', 'delete', 'view'],
            'rules' => [
                [
                    'allow' => true,
                    'actions'=> ['view'],
                    'roles'=>['?']


                ],

                [
                    'allow' => true,
                    'actions'=>[''],
                    'roles' => ['@']


                ],

                [
                    'allow' => true,
                    'actions' => [],
                    'roles' => []

                ],


                [
                    'allow' => true,
                    'actions' => [],
                    'roles' => []



                ],


                [
                    'allow' => true,
                    'actions'=>[],
                    'roles' => []

                ]




            ]
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionFileUpload($storeType = "product", $id = '1') {




        $model = new UploadModel();

        $model->imageCategory = $storeType."/";

        if(Yii::$app->request->isPost){
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $data = $this->findModel($id);


            if($model->upload()){

                $model->oldImageName = $data->image;
                $data->image
                    = $model->imageObj->newName;

                if($model->imageObj->validate()){

                if($data->save()){

                    FileSystem::deleteFile($model->imageObj->getFolderWay() ."/".$model->oldImageName

                    );
                } else {
                    FileSystem::deleteFile($model->imageObj->getFolderWay() .'/'.$model->imageObj->newName
                    );
                }
                    } else {
                    FileSystem::deleteFile($model->imageObj->getFolderWay() .'/'.$model->imageObj->newName);
                }

                $this->redirect(['view','id'=>$id]);
            }
        }

        return $this->render('UploadFileForm', ['model'=>$model]);
    }
}
